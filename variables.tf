#Define variables that we will call
variable "resource_group_name" {
	type = string
  	description = "rg-azureContainerRegistry"
      default= "rg-azureContainerRegistry"
}

variable "acr_name" {
	type = string
  	description = "dContainerRegistry"
      default= "acrContainerRegistry"
}

variable "location" {
	type = string
  	description = "Resources location in Azure"
    default = "WEST Europe"
}
